# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/05/11 18:23:17 by pmartin           #+#    #+#              #
#    Updated: 2017/05/23 06:30:19 by pmartin          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


NAME = RT
CC = cc
FLAGS = -Wall -Wextra -Werror
IDIR = includes/
LMLX = -lSDL2 -lm -lGL -lpthread
LIBFT = libft/
SRC :=		src/calc_main_rt.c \
  			src/calc_sort_inter.c \
	 		src/checker.c \
	  		src/config.c \
			src/detect_cone.c \
  			src/detect_cut.c \
	 		src/detect_cut_light.c \
	  		src/detect_cylinder.c \
			src/detect_disk.c \
 			src/detect_limit.c \
 			src/detect_limit_light.c \
			src/detect_paraboloid.c \
 			src/detect_plan.c \
 			src/detect_sphere.c \
 			src/detect_test.c \
 			src/fresnel.c \
        	src/ft_atof.c \
	        src/get_color.c \
    	    src/get_normal.c \
        	src/get_parser.c \
        	 src/init_main_rt.c \
         	src/init_ray_vp.c \
        	src/init_var.c \
        	src/light_make_color.c \
        	src/light_rfract_rflect.c \
        	src/light_rt.c \
        	src/light_specular.c \
      		src/main.c \
        	src/new_light.c \
        	src/new_mat.c \
        	src/new_obj.c \
        	src/parser_xml.c \
			src/perlin.c \
        	src/perlin_calc.c \
        	src/perlin_init.c \
        	src/pixel_put.c \
        	src/pre_load.c \
       		src/texture.c \
        	src/vec_maths.c \
        	src/vec_maths2.c \
			src/checker2.c \
			src/new_dick.c \
			src/new_glass.c \
			src/new_texture.c \
			src/checkboard.c \

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): $(OBJ)
	@make -C $(LIBFT)
	@$(CC) -o $@ $^ $(FLAGS) $(LIBFT)libft.a $(LMLX) -I $(IDIR)

%.o: %.c
	@$(CC) $(FLAGS) -o $@ -c $< -I $(IDIR)

clean:
	@rm -f $(OBJ)
	@rm -f *~*
	@rm -f *#*

fclean: clean
	@rm -f $(NAME)
	@make -C $(LIBFT) fclean

re: fclean all

.PHONY: all $(NAME) clean fclean re
