/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checkboard.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/23 00:07:16 by pmartin           #+#    #+#             */
/*   Updated: 2017/05/23 05:47:07 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void	make_checkboard(t_var *var, double coef, t_obj *cur, t_light *light)
{
	double	x;
	double	y;

	if (cur->type == 0)
		get_map_sphere(&x, &y, cur);
	else if (cur->type != 1 && cur->type != 4)
		get_map_cylinder(&x, &y, cur, var);
	else
		return ;
	x *= 10000;
	y *= 10000;
	if ((!((int)x % 2) && !((int)y % 2)) || (((int)x % 2) && ((int)y % 2)))
	{
		var->colorf.r += coef * light->color.r * cur->mat.color1.r;
		var->colorf.g += coef * light->color.g * cur->mat.color1.g;
		var->colorf.b += coef * light->color.b * cur->mat.color1.b;
	}
	else
	{
		var->colorf.r += coef * light->color.r * cur->mat.color2.r;
		var->colorf.g += coef * light->color.g * cur->mat.color2.g;
		var->colorf.b += coef * light->color.b * cur->mat.color2.b;
	}
}

void	make_checkboard2(t_var *var, double coef, t_obj *cur, t_light *light)
{
	double	x;
	double	y;

	if (cur->type == 0)
		get_map_sphere(&x, &y, cur);
	else if (cur->type != 1 && cur->type != 4)
		get_map_cylinder(&x, &y, cur, var);
	else
		return ;
	x *= 100000;
	y *= 100000;
	if (!((int)x % 2) && !((int)y % 2))
	{
		var->colorf.r += coef * light->color.r * cur->mat.color1.r;
		var->colorf.g += coef * light->color.g * cur->mat.color1.g;
		var->colorf.b += coef * light->color.b * cur->mat.color1.b;
	}
	else
	{
		var->colorf.r += coef * light->color.r * cur->mat.color2.r;
		var->colorf.g += coef * light->color.g * cur->mat.color2.g;
		var->colorf.b += coef * light->color.b * cur->mat.color2.b;
	}
}
