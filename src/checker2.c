/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcoutinh <mcoutinh@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/02 17:38:14 by mcoutinh          #+#    #+#             */
/*   Updated: 2017/05/23 06:06:56 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <checker.h>

char		*ft_sjoin_del(char *s1, char *s2)
{
	char		*new;
	int			i;
	int			j;

	if (!s1 || !s2)
		return (NULL);
	i = 0;
	while (s1[i])
		i++;
	j = 0;
	while (s2[j])
		j++;
	if ((new = (char *)malloc(sizeof(*new) * (i + j + 1))) == NULL)
		return (NULL);
	i = -1;
	while (s1[++i])
		new[i] = s1[i];
	j = -1;
	while (s2[++j])
		new[i++] = s2[j];
	new[i] = '\0';
	ft_strdel(&s1);
	ft_strdel(&s2);
	return (new);
}

char		*ft_cjoin_del(char *s, char c)
{
	char		*new;
	int			i;

	i = 0;
	while (s[i])
		i++;
	if ((new = (char *)malloc(sizeof(*new) * i + 2)) == NULL)
		return (NULL);
	new[i + 1] = '\0';
	new[i] = c;
	i = -1;
	while (s[++i])
		new[i] = s[i];
	ft_strdel(&s);
	return (new);
}

int			ft_ignore_tag_content(t_checker *c, char **tofree)
{
	char		dquote;
	char		quote;

	ft_strdel(tofree);
	dquote = 'n';
	quote = 'n';
	while ((c->file[++(c->i)] && c->file[c->i] != '>')
			|| dquote == 'y' || quote == 'y')
	{
		if (c->file[c->i] == '\"' && quote == 'n')
		{
			if (dquote == 'n' && c->file[c->i - 1] != '=')
				return (ft_putstr_fd("Error: attribute syntax incorrect\n", 2));
			dquote = (dquote == 'n' ? 'y' : 'n');
		}
		if (c->file[c->i] == '\'' && dquote == 'n')
		{
			if (quote == 'n' && c->file[c->i - 1] != '=')
				return (ft_putstr_fd("Error: attribute syntax incorrect\n", 2));
			quote = (quote == 'n' ? 'y' : 'n');
		}
	}
	if (c->file[c->i - 1] != '/')
		return (ft_putstr_fd("Error: tag syntax incorrect\n", 2));
	return (0);
}
