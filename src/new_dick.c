/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_dick.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/14 12:50:53 by pmartin           #+#    #+#             */
/*   Updated: 2017/05/23 05:44:38 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void		get_vec_text(t_obj *cur)
{
	cur->tool.vx = (t_vec){-cur->dir.z, cur->dir.x, cur->dir.y};
	cur->tool.vy = prodvec(&cur->dir, &cur->tool.vx);
	cur->tool.vx = prodvec(&cur->dir, &cur->tool.vy);
	cur->tool.vy = prodvec(&cur->dir, &cur->tool.vx);
}

static void		new_obj_tag(int fd, t_comp *comp, char *s)
{
	if (ft_strstr(s, "pos"))
		get_vec(s, &comp->pos);
	else if (ft_strstr(s, "dir"))
		get_vec(s, &comp->dir);
	else if (ft_strstr(s, "color"))
		comp->color = get_color(s);
	else if (ft_strstr(s, "height"))
		comp->h = get_cf(s, 't');
	else if (ft_strstr(s, "mat>"))
		comp->mat = new_mat(fd);
	else if (ft_strstr(s, "texture"))
		comp->text = get_c(s, 't');
}

static void		fill_dick2(t_var *var, t_comp *comp)
{
	var->obj[var->nbobj].cut.h1 = (1.0 / 3.0) * comp->h;
	var->obj[var->nbobj + 1].cut.h1 = 0;
	var->obj[var->nbobj].cut.h2 = (1.0 / -3.0) * comp->h;
	var->obj[var->nbobj + 1].cut.h2 = (1.0 / 3.0) * comp->h;
	get_vec_text(&var->obj[var->nbobj]);
	get_vec_text(&var->obj[var->nbobj + 1]);
	get_vec_text(&var->obj[var->nbobj + 2]);
	var->obj[var->nbobj + 2].pos = addvec(comp->pos,
		lenvec(comp->dir, comp->h / -3.8));
	var->obj[var->nbobj + 3].pos = addvec(var->obj[var->nbobj + 2].pos,
		lenvec(var->obj[var->nbobj + 2].tool.vx, comp->h / -3.5));
	var->obj[var->nbobj + 2].pos = addvec(var->obj[var->nbobj + 2].pos,
		lenvec(var->obj[var->nbobj + 2].tool.vx, comp->h / 3.5));
	var->obj[var->nbobj].type = 3;
	var->obj[var->nbobj + 1].type = 0;
	var->obj[var->nbobj + 2].type = 0;
	var->obj[var->nbobj + 3].type = 0;
	var->obj[var->nbobj].color = comp->color;
	var->obj[var->nbobj].scaley = 20;
	var->obj[var->nbobj + 1].color = comp->color;
	var->obj[var->nbobj + 2].color = comp->color;
	var->obj[var->nbobj + 3].color = comp->color;
	var->obj[var->nbobj + 2].mat = comp->mat;
	var->obj[var->nbobj + 3].mat = comp->mat;
	var->nbobj += 4;
}

static void		fill_dick(t_var *var, t_comp *comp)
{
	obj_preload(var, &var->obj[var->nbobj]);
	obj_preload(var, &var->obj[var->nbobj + 1]);
	obj_preload(var, &var->obj[var->nbobj + 2]);
	obj_preload(var, &var->obj[var->nbobj + 3]);
	normvec(&comp->dir);
	var->obj[var->nbobj].dir = comp->dir;
	var->obj[var->nbobj + 1].dir = comp->dir;
	var->obj[var->nbobj + 2].dir = comp->dir;
	var->obj[var->nbobj + 3].dir = comp->dir;
	var->obj[var->nbobj].pos = comp->pos;
	var->obj[var->nbobj + 1].pos = addvec(comp->pos,
		lenvec(comp->dir, comp->h / 3.3));
	var->obj[var->nbobj].r = comp->h / 10;
	var->obj[var->nbobj + 1].r = comp->h / 5;
	var->obj[var->nbobj + 2].r = comp->h / 7;
	var->obj[var->nbobj + 3].r = comp->h / 7;
	var->obj[var->nbobj].text = comp->text;
	var->obj[var->nbobj + 1].text = comp->text;
	var->obj[var->nbobj + 2].text = comp->text;
	var->obj[var->nbobj + 3].text = comp->text;
	var->obj[var->nbobj].mat = comp->mat;
	var->obj[var->nbobj + 1].mat = comp->mat;
	fill_dick2(var, comp);
}

void			new_dick(t_var *var, int fd)
{
	char	*s;
	char	*tmp;
	t_comp	comp;

	s = 0;
	ft_putendl("COMP");
	while (get_next_line(fd, &s) && !(ft_strstr(s, "/dick")))
	{
		tmp = s;
		ft_putendl(s);
		while (*s && *s++ != '<')
			;
		new_obj_tag(fd, &comp, s);
		free(tmp);
	}
	fill_dick(var, &comp);
	free(s);
}
