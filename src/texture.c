/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/17 18:19:49 by pmartin           #+#    #+#             */
/*   Updated: 2017/05/23 07:23:52 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

void			get_map_sphere(double *x, double *y, t_obj *cur)
{
	*x = (acos(d_min(d_max(scal(&cur->tool.n, &cur->tool.vy)
		/ sin(acos(-scal(&cur->dir, &cur->tool.n))), -1.0), 1.0))
		/ (PI * cur->scalex));
	if (scal(&cur->tool.vx, &cur->tool.n) < 0)
		*x = 1 - *x;
	*y = 1 - (acos(-scal(&cur->dir, &cur->tool.n)) / (PI * cur->scaley));
}

void			get_map_cylinder(double *x, double *y, t_obj *cur, t_var *var)
{
	double	v;

	*x = acos(d_min(d_max(scal(&cur->tool.vy, &cur->tool.n), -1.0), 1.0))
		/ (PI * cur->scalex);
	if (scal(&cur->tool.vx, &cur->tool.n) < 0)
		*x = 1 - *x;
	v = 10;
	if (cur->text)
		v = var->text[cur->text - 1]->h / cur->scaley;
	*y = (fmod(scal(&var->raytmp, &cur->dir) * var->inter[0].t1
		+ scal(&cur->tool.vec1, &cur->dir), v) / v);
	if (*y < 0)
		*y = 1 + *y;
	*y = 1 - *y;
}

static void		get_color_text(t_obj *cur, t_var *var, t_color *colorf)
{
	int		color;
	double	x;
	double	y;
	int		x2;
	int		y2;

	if (!var->text[cur->text - 1])
		return ;
	if (cur->type == 0)
		get_map_sphere(&x, &y, cur);
	else if (cur->type != 1 && cur->type != 4)
		get_map_cylinder(&x, &y, cur, var);
	else
		return ;
	if (x < 0 || y < 0)
		return ;
	x2 = x * var->text[cur->text - 1]->w;
	y2 = y * var->text[cur->text - 1]->h;
	ft_memcpy(&color, var->text[cur->text - 1]->pixels
		+ y2 * var->text[cur->text - 1]->pitch + x2 * 3, 3);
	colorf->r = ((color >> 16) & 255) / 255.0;
	colorf->g = ((color >> 8) & 255) / 255.0;
	colorf->b = (color & 255) / 255.0;
}

void			make_text(t_var *var, double coef, t_obj *cur, t_light *light)
{
	t_color	colorf;

	get_color_text(cur, var, &colorf);
	var->colorf.r += coef * light->color.r * colorf.r;
	var->colorf.g += coef * light->color.g * colorf.g;
	var->colorf.b += coef * light->color.b * colorf.b;
}
