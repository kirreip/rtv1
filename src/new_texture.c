/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_texture.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/22 23:05:51 by pmartin           #+#    #+#             */
/*   Updated: 2017/05/23 05:35:34 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static char		*str_clean(char *c)
{
	int			i;
	char		*s;

	i = 0;
	while (*c && *c++ != '"')
		;
	while (c[i] != '"')
		i++;
	if (!(s = ft_strnew(i)))
		return (0);
	i = -1;
	while (c[++i] != '"')
		s[i] = c[i];
	s[i] = 0;
	return (s);
}

void			new_scal(int fd, t_obj *cur)
{
	char		*s;
	char		*tmp;

	while (get_next_line(fd, &s) && !(ft_strstr(s, "/scale")))
	{
		tmp = s;
		if (ft_strstr(s, "scalex"))
			cur->scalex = get_cf(s, 'x');
		else if (ft_strstr(s, "scaley"))
			cur->scaley = get_cf(s, 'y');
		free(tmp);
	}
	free(s);
}

void			new_texture(t_var *var, int fd)
{
	char		*s;
	char		*tmp;
	static int	i;

	s = 0;
	ft_putendl("Parsing CAM");
	while (get_next_line(fd, &s) && !(ft_strstr(s, "/texture")))
	{
		tmp = s;
		while (*s && *s++ != '<')
			;
		if (ft_strstr(s, "new"))
		{
			s = str_clean(s);
			ft_putendl(s);
			var->text[i++] = SDL_LoadBMP(s);
			free(s);
		}
		free(tmp);
	}
	free(s);
}
