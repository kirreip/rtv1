/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_obj.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmartin <pmartin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/14 12:50:53 by pmartin           #+#    #+#             */
/*   Updated: 2017/05/23 07:17:55 by pmartin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv1.h"

static void		get_vec_text(t_obj *cur)
{
	cur->tool.vx = (t_vec){-cur->dir.z, cur->dir.x, cur->dir.y};
	cur->tool.vy = prodvec(&cur->dir, &cur->tool.vx);
	cur->tool.vx = prodvec(&cur->dir, &cur->tool.vy);
	cur->tool.vy = prodvec(&cur->dir, &cur->tool.vx);
	normvec(&cur->tool.vy);
	normvec(&cur->tool.vx);
}

static void		new_cut(int fd, t_cut *cur)
{
	char *s;
	char *tmp;

	while (get_next_line(fd, &s) && !(ft_strstr(s, "/cut")))
	{
		tmp = s;
		if (ft_strstr(s, "pos"))
			get_vec(s, &cur->pos);
		if (ft_strstr(s, "dir"))
			get_vec(s, &cur->dir);
		if (ft_strstr(s, "h1"))
			cur->h1 = get_c(s, '1');
		if (ft_strstr(s, "h2"))
			cur->h2 = get_c(s, '2');
		free(tmp);
	}
	normvec(&cur->dir);
	free(s);
}

static void		new_obj_tag(t_var *var, int fd, t_obj *cur, char *s)
{
	if (ft_strstr(s, "type") && get_type(cur, s))
		var->nbobj++;
	else if (ft_strstr(s, "pos"))
		(ft_strstr(s, "pos2")) ? get_vec(s, &cur->pos2) : get_vec(s, &cur->pos);
	else if (ft_strstr(s, "dir"))
		get_vec(s, &cur->dir);
	else if (ft_strstr(s, "color"))
		cur->color = get_color(s);
	else if (ft_strstr(s, "radius"))
		cur->r = get_cf(s, 'r');
	else if (ft_strstr(s, "negative"))
		cur->neg = get_cf(s, 'v');
	else if (ft_strstr(s, "cut_check"))
		cur->cut_check = get_c(s, 't');
	else if (ft_strstr(s, "cut>"))
		new_cut(fd, &cur->cut);
	else if (ft_strstr(s, "mat>"))
		cur->mat = new_mat(fd);
	else if (ft_strstr(s, "texture"))
		cur->text = get_c(s, 't');
	else if (ft_strstr(s, "scale"))
		new_scal(fd, cur);
}

void			obj_preload(t_var *var, t_obj *cur)
{
	cur->gradobj = var->nbobj;
	cur->mat = var->defaultmat;
	cur->state = 0;
	cur->text = 0;
	cur->cut_check = 0;
	cur->dir = (t_vec){1, 1, 1};
	cur->pos = (t_vec){1, 1, 1};
	cur->color = (t_color){1, 1, 1, 1};
	cur->type = 0;
	cur->r = 1;
	cur->cut.h1 = 0;
	cur->cut.h2 = 0;
	cur->scalex = 1;
	cur->scaley = 100;
}

void			new_obj(t_var *var, int fd, t_obj *cur)
{
	char	*s;
	char	*tmp;

	s = 0;
	obj_preload(var, cur);
	ft_putendl("NEW OBJ");
	while (get_next_line(fd, &s) && !(ft_strstr(s, "/object")))
	{
		tmp = s;
		ft_putendl(s);
		while (*s && *s++ != '<')
			;
		new_obj_tag(var, fd, cur, s);
		free(tmp);
	}
	normvec(&cur->dir);
	if (cur->type == 2)
		cur->tool.k = tan(3.1415 * (cur->r / 360.0));
	get_vec_text(cur);
	free(s);
}
